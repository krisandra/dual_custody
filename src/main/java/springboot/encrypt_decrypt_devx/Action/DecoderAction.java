package springboot.encrypt_decrypt_devx.Action;

import java.util.HashMap;
import java.util.Map;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import springboot.encrypt_decrypt_devx.App;

@Component
public class DecoderAction {
	
	static Logger log = LoggerFactory.getLogger(DecoderAction.class);
	private static StandardPBEStringEncryptor s_encryptor = null;
	
	@Value("${dual.custody.algorithm}")
	String algorithm;
	
	@Value("${dual.custody.password}")
	String password;
	
	public Map EncryptTaxt(String planText) {
		Map resultMap = new HashMap<>();
		String encryptedString = "";
//		String algorithm = "PBEWithMD5AndDES";
//		String password = "CimbSalt69devx";
		log.info("algorithm: "+algorithm);
		log.info("password: "+password);
		log.info("planText: "+planText);

         s_encryptor = new StandardPBEStringEncryptor();
         s_encryptor.setAlgorithm(algorithm);
         s_encryptor.setPassword(password);
         
         
         try {
			encryptedString = s_encryptor.encrypt(planText);
			log.info("encryptedString: "+encryptedString);
			resultMap.put("responseCode","00");
			resultMap.put("responseMessage","Encrypt data success");
			resultMap.put("encryptData",encryptedString);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resultMap.put("responseCode","01");
			resultMap.put("responseMessage","Error Encrypt data");
			
		}
         log.info("resultMap: "+resultMap);
		return resultMap;
	}
	
	public Map DecryptTaxt(String planText) {
		Map resultMap = new HashMap<>();
		String decryptedString = "";
//		String algorithm = "PBEWithMD5AndDES";
//		String password = "CimbSalt69devx";
		log.info("planText: "+planText);

         s_encryptor = new StandardPBEStringEncryptor();
         s_encryptor.setAlgorithm(algorithm);
         s_encryptor.setPassword(password);
         
         
         try {
			decryptedString = s_encryptor.decrypt(planText);
			log.info("decryptedString: "+decryptedString);
			resultMap.put("responseCode","00");
			resultMap.put("responseMessage","Decrypt data success");
			resultMap.put("decryptData",decryptedString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resultMap.put("responseCode","01");
			resultMap.put("responseMessage","Error Decrypt data");
			
		}
         log.info("resultMap: "+resultMap);
		return resultMap;
	}
	
	public Map checker(String encrypt, String planText) {
		Map resultMap = new HashMap<>();
		String decryptedString = "";
//		String algorithm = "PBEWithMD5AndDES";
//		String password = "CimbSalt69devx";
		log.info("encrypt: "+encrypt);

         s_encryptor = new StandardPBEStringEncryptor();
         s_encryptor.setAlgorithm(algorithm);
         s_encryptor.setPassword(password);
         
         
         try {
			decryptedString = s_encryptor.decrypt(encrypt);
			log.info("decryptedString: "+decryptedString);
			log.info("planText: "+planText);
			
			if(decryptedString.equals(planText)) {
				resultMap.put("responseCode","00");
				resultMap.put("responseMessage","your data macth");
			}else {
				resultMap.put("responseCode","99");
				resultMap.put("responseMessage","your data doesn't macth");
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resultMap.put("responseCode","01");
			resultMap.put("responseMessage","Error Decrypt data");
			
		}
         log.info("resultMap: "+resultMap);
		return resultMap;
	}

	
	public void main( String[] args )
	{
		System.out.println("test: "+EncryptTaxt("jancuk1234"));
		System.out.println("test: "+DecryptTaxt("rBTtH1JzTDdit2M7tc/jcQ=="));
		
	}
}
