package springboot.encrypt_decrypt_devx.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import springboot.encrypt_decrypt_devx.Action.DecoderAction;

@RestController
public class DecoderController {

	@Autowired
	DecoderAction DecoderAction;
	Logger log = LoggerFactory.getLogger(DecoderController.class);
	
	 @GetMapping("/DecoderInput")
	    public String DecoderInput(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
	        model.addAttribute("name", name);
	        return "DecoderInput";
	    }
	
	@RequestMapping(value="/decoder/encrypt", method = RequestMethod.POST)
	ResponseEntity<Map> encrypt (@RequestBody Map data) {
		log.info("data: "+data);
			Map resultMap = new HashMap<>();
			String planText = (String) data.get("planText");
			log.info("planText: "+planText);
			String encrypt = "";
		if (!"".equals(planText) || !planText.isEmpty()) {
			resultMap = DecoderAction.EncryptTaxt(planText);
			 return  (ResponseEntity<Map>) ResponseEntity.status(HttpStatus.OK).body(resultMap);
		}else {
			resultMap.put("responseCode","02");
			resultMap.put("responseMessage","plan Text null Error Decrypt data");
			return  (ResponseEntity<Map>) ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMap);
		}
	}
	@RequestMapping(value="/decoder/decrypt", method = RequestMethod.POST)
	  ResponseEntity<Map> decrypt (@RequestBody Map data) {
		Map resultMap = new HashMap<>();
		String planText = (String) data.get("planText");
		log.info("planText: "+planText);
		String encrypt = "";
		if (!"".equals(planText) || !planText.isEmpty()) {
			resultMap = DecoderAction.DecryptTaxt(planText);
//			return resultMap;
			  return  (ResponseEntity<Map>) ResponseEntity.status(HttpStatus.OK).body(resultMap);
		}else {
			resultMap.put("responseCode","02");
			resultMap.put("responseMessage","plan Text null Error Decrypt data");
			return  (ResponseEntity<Map>) ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMap);
		}
	}
	
	@RequestMapping(value="/decoder/checker", method = RequestMethod.POST)
	  ResponseEntity<Map> checker (@RequestBody Map data) {
		Map resultMap = new HashMap<>();
		String planText = (String) data.get("planText");
		log.info("planText: "+planText);
		String encrypt = (String) data.get("encrypt");
		log.info("encrypt: "+encrypt);
		
		if (!"".equals(planText) || !planText.isEmpty()) {
			resultMap = DecoderAction.checker(encrypt,planText);
//			return resultMap;
			  return  (ResponseEntity<Map>) ResponseEntity.status(HttpStatus.OK).body(resultMap);
		}else {
			resultMap.put("responseCode","02");
			resultMap.put("responseMessage","plan Text null Error data");
			return  (ResponseEntity<Map>) ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMap);
		}
	}	
}
