package springboot.encrypt_decrypt_devx.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Decoder {
	@NotBlank
	@Size(max = 100)
	private String encrypt;
	
	@NotBlank
	@Size(max = 100)
	private String decrypt;

	public String getEncrypt() {
		return encrypt;
	}

	public void setEncrypt(String encrypt) {
		this.encrypt = encrypt;
	}

	public String getDecrypt() {
		return decrypt;
	}

	public void setDecrypt(String decrypt) {
		this.decrypt = decrypt;
	}
	
	
}
